#include <stdlib.h>
#include <iostream>
#include <string>

#if defined(_WIN32) || defined(WIN32)
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

#include <Windows.h>
#include <ShellApi.h>

int main(int argc, char * argv[]) {
	int err;
	err = (int) SetCurrentDirectory("data");
	if (err == 0) std::cout << "SetCWD error." << std::endl;

	err = (int) ShellExecute(NULL, "open", "..\\engine\\love.exe", ".", NULL, SW_SHOW);

	if (err < 32) {
		std::cout << "Error " << err;
		std::string msg = ". ";
		if (err == ERROR_FILE_NOT_FOUND || SE_ERR_FNF) msg = msg + "File not found. ";
		if (err == ERROR_PATH_NOT_FOUND || SE_ERR_PNF) msg = msg + "Path not found. ";
		if (err == ERROR_BAD_FORMAT) msg = "Bad format. ";
		if (err == SE_ERR_ACCESSDENIED) msg = "Acces denied. ";
		if (err == 0) msg = "Filesystem out of memory or resources. ";
		std::cout << msg << std::endl;
	}
	return 0;
}

#endif