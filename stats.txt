Mana is automatically restores 5% each turn, potions can increase the rate for some turns.
Health is automatically restored 1% each turn, potions can increase the rate for some turns.

Combat Skills (Menu):
      Attack
Defense    Magic
      Leave

|Mana| - |HP| - |Stamina| (Like in skyrim)

			(These modify the skill level C=%CurrentLevel)
Defense:		ATACK	DEFENSE	EVASION		ENDURANCE (Affects stamina consumption)
- Balanced		0	  0		      0		      0
- Evasion		  1	  -3        3   		  -1
- Defensive		-3	3		      -3    		3
- Offensive		3	  -1    		-1    		-1
- Magic Protection	0	2		0		-1 (Consumes 20% mana each turn)

Attack: ()
Swing | Hit | Shot (Use main weapon)
Strong Swing | Hit | Shot (Like before but +50%, consumes stamina)

Magic:


Skill levels: (The % is applied to the Player stats as in the table above)
0.1% - Zero
2%   - First Steps
5%   - Novice
10%  - Principiant
20%  - Aprentice
30%  - Student
40%  - Talented
50%  - Good Student
60%  - Proficent
70%  - Teacher 
80%  - Expert Teacher 
90%  - Master
99%  - Great Master 
120% - Legendary (Blocked Until All Levels At Great Master)
999% - Divine (Blocked until Badass Ending Route)


STAT TABLE
Wd = Weapon Damage
Ar = Armature Protection

Type        BStat         Buff     Lvl      LvlBuff  FStat
Attack      Wd || 200     +0       1 (2%)   +0       20*0.02
Defense     Sum(Ar)       +0       1 (2%)   +0       0*0.02 
Evasion     100           +0       1 (2%)   +0       100*0.02
Endurance   100(%Weight)  +0       1 (2%)   +0       90*0.02

Damage = Attack - Enemy.Defense/math.random(1,2,0.01) (>=0)
