================================================================================
                                                 Journey Through Izildia II - War Time
================================================================================

TODO LIST
--------------------------------------------------------------------------------

::Feaures::

Priority 	Feature
1		Quest Log in the bag.
2		Implement GUI for inventory and skills.
1		Make decision dialog (Horizontal white line with text and buttons (horizonatl bottom) ). For now it will just be a menu.
1		Images on books (for example a book of character cards would be amazing)
2		Exit game dialog.
3		Day night cycle (safe save, etc...) On the map scene. On certain location you can chose to "Go to sleep" and similar.
2		Reduce the amount of reandomness on the game, let the player choose instead.
2		Next button (reenter location) and stats button on the map scene.
3		Optimize game on Andorid, entering the slideshow scene takes forever.
2		Make and android launcher instead of using the .love file.
2		Major code cleaning, a lot of things could be done better, that's for sure (battle script, effects, loveframes, I'm looking at you)

::Content::
- Random battles at training camp (to improve)
- Temple story.
- Encounter with Aevys at river.
- You discover Aevys is the culprit for the behavior of that bear. Also you discover the hideout of the bear and battle with bear.
- Night faprulette with Cleo.

Misc
--------------------------------------------------------------------------------
The sorceress of the first game should have a major role on the second.

Remeber before pushing changes to the remote repository
--------------------------------------------------------------------------------
- Quest states are the initial states (quest/main/init.lua chapter=1, state=0)
- Any feature you deactivated has been reactivated again.
- Test that everything at least seems to work.



