Journey Through Izildia II: Wartime
-----

Welcome to the JTI2 repository. 

Everybody is free to make aportations, but please make sure to stick to the TODO list (script.txt), if you want to make an aportation that's not on the TODO send me a mail first.

Exempt to the avobe are in-game books, grammar correction and translations. 

How to use the editor
-----

When the editor is enabled (For now use: slideshow.lua, self.editmode = true) you can use it to edit text of any slide. The controls are the following:

- Click and move on text: Moves it. Position is not actually saved until you leave the edit mode.
- Ctrl+E: Edit mode, let's you edit text.
- Ctrl+W/Ctrl+Q: Change text wide.
- Ctrl+A/Ctrl+S: Change text hight.
- Ctrl+R: Open slide options (change background, images, etc...)

Notice that all of this is WIP and may substantially change on the future.