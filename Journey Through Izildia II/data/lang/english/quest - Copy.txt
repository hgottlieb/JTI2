local _={}
_[167]={630,441}
_[166]={331,100}
_[165]={332,44}
_[164]={366,153}
_[163]={559,106}
_[162]={562,52}
_[161]={289,139}
_[160]={667,382}
_[159]={291,84}
_[158]={620,464}
_[157]={327,64}
_[156]={330,9}
_[155]={291,203}
_[154]={596,315}
_[153]={298,145}
_[152]={519,153}
_[151]={428,272}
_[150]={519,98}
_[149]={454,439}
_[148]={340,58}
_[147]={18,6}
_[146]={47,412}
_[145]={616,127}
_[144]={45,363}
_[143]={530,392}
_[142]={353,110}
_[141]={355,55}
_[140]={490,200}
_[139]={225,491}
_[138]={380,449}
_[137]={490,200}
_[136]={292,140}
_[135]={288,72}
_[134]={52,376}
_[133]={855,142}
_[132]={325.38799414348466,316.25183016105416}
_[131]={84,346}
_[130]={783,180}
_[129]={409,291}
_[128]={28,454}
_[127]={631,68}
_[126]={24,411}
_[125]={490,200}
_[124]={258,468}
_[123]={363,425}
_[122]={471,102}
_[121]={471,200}
_[120]={474,44}
_[119]={578,412}
_[118]={338,98}
_[117]={338,53}
_[116]={569,139}
_[115]={351,201}
_[114]={567,96}
_[113]={378,86}
_[112]={505,385}
_[111]={378,36}
_[110]={800,129}
_[109]={56.925329428989755,489.83894582723281}
_[108]={59,448}
_[107]={327,64}
_[106]={620,471}
_[105]={330,9}
_[104]={667,382}
_[103]={289,139}
_[102]={291,84}
_[101]={730,343}
_[100]={125.79795021961934,127.90629575402636}
_[99]={240,16}
_[98]={730,316}
_[97]={125.79795021961934,127.90629575402636}
_[96]={240,16}
_[95]={879,143}
_[94]={40,379}
_[93]={37,334}
_[92]={position=_[166],size=_[167],text="Just after entering the hall you spot Ayka comming at you.\n\n#5_Thank godess, you are okey, I was worried you encountered that beast on your way back.\n\n#6_You know about the attack?\n\n#5_Yes, Aevys informed us, we were very lucky that she was near. \n\n#6_Yes, what was Aevys doing there anyway, shoudn't she be at the meeting?\n\n#5_I don't know, she consigned Arin to be the head on the meetings and now she speends most of her time at the temple or walking on the forest.\n\n#6_Ohh, is that so, how inusual. Then what are we going to do about the bear?\n\n#5_Listen Alice, that bear is dangerous, don't even try to hunt him down, if you encounter him, flee imediatly.\n\n#6_What? But we need to do something, that bear hurted Lithia --\n\n#5_Lith--! ... Anyway, don't try to hunt him. Aevys will probably find him soon and kill him for good, so don't worry and make sure to keep training. I have to return to the meeting now, good bye."}
_[91]={position=_[165],text="Assembly Hall"}
_[90]={y="250",x="50",path="npc/ayka_normal.png"}
_[89]={position=_[163],size=_[164],text="You are at the huts.\n\nYou relax and stay for awhile in your comfy home."}
_[88]={position=_[162],text="Huts"}
_[87]={size=_[160],position=_[161],text="When you reach the huts you see a conglomeration of people gathered arround the herbalist house. You let the boar at the designed place alongside the kitchen and hurry up to go see what's going on.\n\nInside the cottage there's a known face, a child called Lithia, your friend and Aykas little sister.\n\n#6_Lithia! Are you ok? What happened?\n\n#8_Oh Alice, you see, a monster appeard, it was huge! an it attaked randomly everybody here at the huts. No one was killed but a lot are injured. I'm worried Alice, what if it comes again?\n\n#6_That's terrible. Does Ayka know about this?\n\n#8_I don't know, I have not seen here since morning.\n\n#6_Then if Ayka wasn't here, who drove away the creature?\n\n#8_It was Aevys! She used a strange spell I'd never seen and the bear ran away as fast as he could.\n\n#6_So Aevys, the elf leader, did it. Wait a moment, a bear?"}
_[86]={position=_[159],text="Commotion"}
_[85]={y="250",x="50       ",path="npc/lithia_worried.png"}
_[84]={position=_[157],size=_[158],text="Welcome to the assembly hall.\n\nYou find yourself surrounded with merchants and important looking people all arround. Most of them come from noble families and are here for the meetings being celebrated this week in favor of the prosperity of the land. \n\nThis meetings are celebrated every year and they reunite elfs from all the forest, a forest that extends hundreds of kilometers in all directions.\n\n#5_Hello Alice, oh I see you're in company of our youngest member, nice to see you too Cleo.\n\nThe elfs being here aren't simple elfs, usually they are descendents of the great elf hero or people with notorious influence.\n\n#7_Hello Ayka.\n\n#6_Hi Ayka, I came to report the succes of my last mission.\n\nThis one tough is Ayka, your intstructor from the warriors guild, wich is also a memeber of the elders table."}
_[83]={position=_[156],text="Assembly Hall"}
_[82]={y="250",x="50",path="npc/ayka_greet.png"}
_[81]={size=_[154],position=_[155],text="#8_It was a really huge bear! And it was terrifying too...\n\n#6_Ok, ok, don't worry, I'm sure Ayka will do something about it as soon as she knows about it. She may even send me to hunt him!\n\n#8_Alice... be careful ok?\n\n#6_Don't worry, I'll be fine! More important, I should go find Ayka.\n\nIt seems you got a new hunting mission just after finishing the last one, and it looks important too!"}
_[80]={position=_[153],text="Commotion"}
_[79]={y="250",x="50",path="npc/lithia_worried.png"}
_[78]={size=_[151],position=_[152],text="This is yours and Cleo's home. It's a confy cotage with water supply and everyday utensils. As it's common in the village the door is left open all day and sometimes you find unexpected visitors inside.\n\nThere is plenty of wierd forniture, Cleo always brings more and more. Everytime she says she bought if from a merchant who told her it came from some remote place outside the forest or something like that.\n\nIf she continues bringing more wierd things we will have to start sleeping on the roof."}
_[77]={position=_[150],text="Home"}
_[76]={y="0",x="0",path=""}
_[75]={position=_[148],size=_[149],text="#spawn:wild-boar_\n\n#spawn:wild-boar_"}
_[74]={position=_[147],text="Hunting for dinner!"}
_[73]={y="330",x="30",path="npc/wild_boar.jpg"}
_[72]={size=_[145],position=_[146],text="There seems to be battles going on, maybe I should join them?"}
_[71]={position=_[144],text="Training Camp"}
_[70]={y="0",x="0",path=""}
_[69]={position=_[142],size=_[143],text="You move on top of her and remove the sheet. You place your head beetwhen her legs and pull you tonge twards her pussy. Cleo makes a surprissed face as you suddenly start liking her womahood, some moans scape her lips. Soon only a blissful expression remains.\n\nShortly after, you start to masturbate yourself, your cunt already soaked with your fluids.\n\n#task_Apply lube to your boypussy and start fingering yourself, use at last X fingers.\n\nOnce you both are satisfied it's finally time to sleep. You hold Cleo in your embrace and close your eyes."}
_[68]={position=_[141],text="Back at home"}
_[67]={position=_[139],size=_[140],text="The steam flows at a normal speed. You can't hear any animal near."}
_[66]={position=_[138],text="The river"}
_[65]={position=_[136],size=_[137],text="#spawn:basic-elf_"}
_[64]={position=_[135],text="Training at the camp"}
_[63]={y="0",x="0",path=""}
_[62]={size=_[133],position=_[134],text="You're walking in the woods near the river. You aproach the river and is then when you notice it, you aren't alone. Some meters height there's a family of wild boars, they are pacefully drinking from the river.\n\nAs your instructor told you, your mission is to hunt them. This will be the first time you try your skills in real combat. You notice your hand trembling, you try to relax and start creeping to the underbrush stalking them.\n\nA few moments leter the time for battle has come, you reveal your hideout and attack them!"}
_[61]={position=_[132],text="A Wild Boar Appears!"}
_[60]={y="",x="",path=""}
_[59]={size=_[130],position=_[131],text="Finally both wild boars rest on the ground dead. You take the little one with one hand and tie his pasterns together. You then pull from the extreme of the rope lifting the animal from it's resting place, he is gonna be the dinner tonight.\n\nThe bigger one is too heavy for you alone and you have no other choice but to let it there. Some other predator will find it and feed from it, then he will grow big and healty and will be your dinner next time.\n\nEven being the little one you struggle to carry it all the way, you need to make multiple breaks along the path to rest for a while. After what seems a much longer walk than what you expected, you finally see your destination."}
_[58]={position=_[129],text="Victory!"}
_[57]={y="0",x="0",path=""}
_[56]={size=_[127],position=_[128],text="There seems to be more fighters waiting, one more battle won't hurt... right?"}
_[55]={position=_[126],text="Training camp"}
_[54]={y="0",x="0",path=""}
_[53]={position=_[124],size=_[125],text="You are at the forst edge, you aren't allowed to go any further."}
_[52]={position=_[123],text="Forest Border"}
_[51]={size=_[121],position=_[122],text="Welcome to the assembly hall.\n\nYou see people walking arround leisurely. There doesn't seem to be any problems at the market.\n\nAt the end of the hall there is the meeting room, Cleo is probaby there, you shouldn't disturb her."}
_[50]={position=_[120],text="Assembly Hall"}
_[49]={position=_[118],size=_[119],text="#7_Welcome back Alice, come on, get in.\n\n#6_You want me to sleep with you again, what a spoiled child you are.\n\n#7_Don't call me a child, I have an important position now.\n\n#6_Yeah, you're right. How has it been at your meeting with the elders?\n\n#7_They couldn't care less about my opinion, they just started talking and talking wihout making any point, they were just rambling. I think they only want me there becouse of my lineage.\n\n#6_You endured well...\n\n#7_Yes I did, it was hard you know. I demand compensation.\n\nShe says playfully with an alluring face. She aproaches you under the sheet and moves her knee to your crotch, placing her leg between yours.\n\n#6_I see."}
_[48]={position=_[117],text="Back at home"}
_[47]={size=_[115],position=_[116],text="You arrive at your confy home and sit down to relax when... savage book appears!\n\nThis must be one of Cleo's Book that's left over your armchair... hmm maybe it is interesting."}
_[46]={position=_[114],text="At home"}
_[45]={y="0",x="0",path=""}
_[44]={size=_[112],position=_[113],text="#7_Uaahhh ahhahah...\n\nCleo wakes up extending her arms to the ceiling avobe her head. You look at her, happy to have her at your side one more day.\n\n#6_Good morning sweetie.\n\n#7_Good morning Alice. Today I have another meeting...  this time, this time I will convince them! We need to stop being an isolated land unaware of the external world.\n\n#6_Then I will accompany you for a while. Ayka will be there and I just finished my last mission.\n\nYou both dress up and go out. Time to head to the assambly hall."}
_[43]={position=_[111],text="Good morning"}
_[42]={position=_[109],size=_[110],text="There doesn't seem to be anyone to train with right now. You should come back later."}
_[41]={position=_[108],text="Trainig camp"}
_[40]={y="0",x="0",path=""}
_[39]={size=_[106],position=_[107],text="#5_So you finaly did it uhm...\n\n#6_Yes, ten hits at the bull's-eye in a row.\n\n#5_Good. Ok then I hope you're energetic today becouse it's time for your first real mission.\n\n#6_Finally! What will it be? Scorting an important leader? Exploring the forest border? Taking down a group of thiefs?\n\n#5_Nope, we need a lot of food for the festival this night and we are short in supplies, so you will go hunting.\n\n#6_A hunting job...\n\n#5_Yes, go to the river, a lot of animals gather there when they are thirsty, try to capture porc or goat.\n\n#6_Ahh... ok! Ayka, I'll bring you the greatest porc there is out there, you'll see.\n\nYou reluctantly yet with entusiasm accept your new quest."}
_[38]={position=_[105],text="Assembly Hall"}
_[37]={y="250",x="50",path="npc/ayka_normal.png"}
_[36]={position=_[103],size=_[104],text="When you reach the huts you see a conglomeration of people gathered arround the herbalist house. You let the boar at the designed place alongside the kitchen and hurry up to go see what's going on.\n\nInside the cottage there's a known face, a child called Lithia, your friend and Aykas little sister.\n\n#6_Lithia! Are you ok? What happened?\n\n#8_Oh Alice, you see, a monster appeard, it was huge! an it attaked randomly everybody here at the huts. No one was killed but a lot are injured. I'm worried Alice, what if it comes again?\n\n#6_That's terrible. Does Ayka know about this?\n\n#8_I don't know, I have not seen here since morning.\n\n#6_Then if Ayka wasn't here, who drove away the creature?\n\n#8_It was Aevys! She used a strange spell I'd never seen and the bear ran away as fast as he could.\n\n#6_So Aevys, the elf leader, did it. Wait a moment, a bear?"}
_[35]={position=_[102],text="Commotion"}
_[34]={y="250",x="50       ",path="npc/lithia_worried.png"}
_[33]={position=_[100],size=_[101],text="You pour the river's water from your hand and let it flow down through your pale skin and on the curves of your body. You caress your crotch feeling the softness of your hairless pussy. \n\nFrom the distance the siluete of a beautiful female can be percieved, pointy ears pretuding from her head, and a long black hair that reaches past her shoulders.\n\n#task_Shave all your body and take a shower with templated water.\n\nOnce your feel like you're clean enough you prepare to go back to the camp. It's late already and you're sure there'll be someone waiting for you."}
_[32]={position=_[99],text=""}
_[31]={position=_[97],size=_[98],text="You find yourself in this ancient forest. A paceful place filled with magic and populated with creatures of all kinds. A remote land privated from the nuisance of civilization. \n\nWithin the calm of the night you hear the bubbling sound of a nearby stream. You move towards the origin of the sound until you finally reach a river. \n\nThe moon illuminates the background and reflects itself on the surface of the crystalline water.\n\nYou examine the surroundings, it doesn't seem like there is anyone arround. Deciding that this is indeed a good place you start undressing yourself.\n\nYou enter nude into the river..."}
_[30]={position=_[96],text=""}
_[29]={position=_[94],size=_[95],text="You're at the training camp. The camp is a sactuary situated inside a natural cabern created by the constant flow of water at the center. The entrance to the building holding the secret of the source is protected by an unbreakable spell. It's told that only the leader of the elfs, Aevys, can disable the spell and enter the sealed chamber, nobody but her knows what kind of magic resides inside.\n\nAt the outside of the sealed chamber there is the trainig camp, you've been training here for months before you were given your first mission, that doesn't mean that you don't need more training tough."}
_[28]={position=_[93],text="Training Camp"}
_[27]={y="0",x="0",path=""}
_[26]={aux=_[90],background="map/assembly_hall.jpg",title=_[91],text=_[92]}
_[25]={background="map/huts.jpg",title=_[88],text=_[89]}
_[24]={aux=_[85],background="one/huts_outside.jpg",title=_[86],text=_[87]}
_[23]={aux=_[82],background="map/assembly_hall.jpg",title=_[83],text=_[84]}
_[22]={aux=_[79],background="one/huts_outside.jpg",title=_[80],text=_[81]}
_[21]={aux=_[76],background="map/huts.jpg",title=_[77],text=_[78]}
_[20]={aux=_[73],background="map/river.jpg",title=_[74],text=_[75]}
_[19]={aux=_[70],background="map/training_camp.jpg",title=_[71],text=_[72]}
_[18]={background="intro/intro3.jpg",title=_[68],text=_[69]}
_[17]={background="map/river.jpg",title=_[66],text=_[67]}
_[16]={aux=_[63],background="map/training_camp.jpg",title=_[64],text=_[65]}
_[15]={aux=_[60],background="map/river.jpg",title=_[61],text=_[62]}
_[14]={aux=_[57],background="map/river.jpg",title=_[58],text=_[59]}
_[13]={aux=_[54],background="map/training_camp.jpg",title=_[55],text=_[56]}
_[12]={background="map/forest_border.jpg",title=_[52],text=_[53]}
_[11]={background="map/assembly_hall.jpg",title=_[50],text=_[51]}
_[10]={background="intro/intro2.jpg",title=_[48],text=_[49]}
_[9]={aux=_[45],background="map/huts.jpg",title=_[46],text=_[47]}
_[8]={background="intro/intro4.jpg",title=_[43],text=_[44]}
_[7]={aux=_[40],background="map/training_camp.jpg",title=_[41],text=_[42]}
_[6]={aux=_[37],background="map/assembly_hall.jpg",title=_[38],text=_[39]}
_[5]={aux=_[34],background="one/huts_outside.jpg",title=_[35],text=_[36]}
_[4]={background="intro/intro1.jpg",title=_[32],text=_[33]}
_[3]={background="intro/intro1.jpg",title=_[30],text=_[31]}
_[2]={aux=_[27],background="map/training_camp.jpg",title=_[28],text=_[29]}
_[1]={fta_training_camp=_[2],main_intro_0=_[3],main_intro_1=_[4],main_ch1_hugebear_1=_[5],main_ch1_1=_[6],default_trainingcamp_0=_[7],main_intro_4=_[8],map_huts_book=_[9],main_intro_2=_[10],default_assembly_0=_[11],default_forestedge_0=_[12],main_ch1_trainingcamp_battle_more=_[13],main_ch1_3=_[14],main_ch1_2=_[15],battle_trainingcamp_elf=_[16],default_river_0=_[17],main_intro_3=_[18],main_ch1_trainingcamp_battle_intro=_[19],battle_river_wildboar=_[20],fta_huts=_[21],main_ch2_hugebear_2=_[22],main_ch1_0=_[23],main_ch1_hugebear_2=_[24],default_huts_0=_[25],main_ch1_5=_[26]}
return _[1]