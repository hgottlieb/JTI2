local self = {}
local q = nil

local tut_wildboar = false
local menu = nil

local songlist = {
  map = "Tribal Jungle Music - Jungle Festival.mp3",
  battle = "Antti Martikainen - From the Fields of Gallia.mp3"
}

function self.init(kst)
  q = kst
  self.times = {}
end

function self.load()
  -- Loading extras
  if q.state >= 2 then
    quest.playSong(songlist.map)
  end
end

local st = {temple = 0}

function self.enterLocation(loc)
  if q.state == 0 then
    q.state = 1
    quest.pushBack("main_intro_0")
    quest.pushBack("main_intro_1")
    music.source = love.audio.newSource("sound/effect/forest_night.ogg")
    music.source:setLooping(true)
    music.volume = 0
    music.source:setVolume(music.volume)
    Timer.tween(5, music, {volume=1})
    music.source:play()
    player.loc = "river"
  elseif q.state == 1 then
    quest.pushBack("main_intro_2")
    quest.pushBack("main_intro_3")
    quest.pushBack("main_intro_4")
  elseif q.state == 2 then
    quest.pushBack("main_ch1_0")
    quest.pushBack("main_ch1_1")
    q.state = 3
  elseif q.state == 3 then
    if loc == "river" then
      Effect.fadeToSong(songlist.battle)
      quest.pushBack("main_ch1_2")
      quest.pushBack("battle_river_wildboar")
    end
  elseif q.state >= 4 then
    if loc == "huts" then
      if not st.hugebear then
        quest.pushBack("main_ch1_hugebear_1")
        quest.pushBack("main_ch2_hugebear_2")
        st.hugebear = 1
      elseif st.hugebear == 1 then
        if math.random(1,5) == 1 then
          quest.pushBack("main_ch1_hugebear_3")
        end
      end
    elseif loc == "assembly_hall" and q.state == 4 then --not 4, why? and what then?
      quest.pushBack("main_ch1_5")
      q.state = 5
    elseif loc == "training_camp" then
      if math.random(1,4) > 0 then
        quest.pushBack("main_ch1_trainingcamp_battle_intro")
      end
    end
  end
  
  if quest.isEmpty() and loc == "huts" and (quest.times["huts"] or 0) >= 1 then
    if math.random(1, 5) == 1 then
      quest.pushBack("map_huts_book")
    end
  end
end

function self.exitLocation(loc)
  if q.state == 1 then
    if loc == "river" then
      player:moveTo("huts")
      Timer.tween(2, music, {volume=0})
    end
    if loc == "huts" then
      player:moveTo("assembly_hall")
      q.state = 2
    end
  elseif (q.state == 3 or q.state == 4) and loc == "river" then
    Effect.fadeToSong(songlist.map)
    if battle.result == "win" then
      player:moveTo("huts")
    end
  end
  
  if q.state > 1 then quest.playSong(songlist.map) end
end

function self.nextScene(id)
  if menu then menu:Remove() end
  
  if id == "map_huts_book" then
    menu = control.create_menu("Read book")
    menu.b[1].OnClick = function() book.new("book_izildiaguide.txt"):Open() end
  end
  
  if id == "main_ch1_trainingcamp_battle_intro" or id == "main_ch1_trainingcamp_battle_more" then
    menu = control.create_menu("Join Training")
    menu.b[1].OnClick = function()
      quest.insert(1, "battle_trainingcamp_elf")
      scene.list.slideshow.goNext()
    end
  end
  
  if id == "main_intro_0" then
    --scene.list.slideshow.itext.colormask = {{255,255,255,0}}
    --Effect.Timer:after(6, function() Effect.textFadeInParagraphs() end)
    Effect.addAfterLast("wait", 6)
    Effect.addAfterLast("textFadeInParagraphs")
    Effect.addBeforeNext("textFadeOutParagraphs", 2)
  
elseif id == "main_intro_1" then
    Effect.makeColormaskInvisible(scene.list.slideshow.itext)
    control.MsgBox("You've got a task!\n\nTasks are marked with a diferent font and color, you're supposed to do them on real life.\n\nYou can disable tasks on the configuration menu.", "Tutorial", nil, function() Effect.textFadeInParagraphs() end)
    
  elseif id == "main_intro_3" then
    control.MsgBox("At the bottom-right of the screen you have now a set of letters and numbers, that's your roll.\n\nMost tasks use parameters to detereminate certain actions.\n\nSometimes when a task doesn't specify an important parameter you can use your roll to find it. The default paramaters are:\n\n\t(W) - Undefined\n\t(V) - Undefined\n\t(X) - Undefined\n\t(Y) - Duration in minutes\n\t(Z) - Undefined", "Tutorial")
    player.dice:roll("wvxyz")
    player.dice.y = math.random(5,9)
    player.dice.x = math.random(2,3)
    Effect.addBeforeNext("blackFadeIn", 2)
  
  elseif id == "main_intro_4" then
    Effect.addAfterLast("wait", 2) -- Create an effect that shows a text, make the text say "Morning".
    Effect.addAfterLast("invokeText", "Chapter One", "Start of a new day", 4, 5, 2)
    Effect.addAfterLast("blackFadeOut", 5)
    quest.playSong(songlist.map)
    player.dice:nilify()
    
  elseif id == "battle_river_wildboar" then
    battle.getNPC(2).hp = {100, 100, 1}
    battle.refresh()
    if tut_wildboar == false then
      local frame = control.MsgBox("You are on battle mode. Notice the new menu at the bottom right, you can select you actions from there. \n\nThe battle system is turn-based, the actions of everyone will be registred on the main text.\n\nRight now you're facing two wild boars, the second one seems to have less HP (health points), it is often a good idea to go first for the weak.", "Tutorial", nil, function()
        local frame = control.MsgBox("To win this battle you will need to use <Stance>. Changing your stances will effect you control level.\n\nFor example the a defensive stance will make your defensive skills temporary better while reducing your attack power. Your evasive stance will make your armature useless but you will be able to avoid some attacks completly much more easily. The ofensive stance will make your attacks much greater, but be wary with your stamina consumption as great attacks have a cost.\n\nOn this battle you will need to use all three basic stances wisely, good luck.", "Tutorial")
        loveframes.SetState("Battle")
        frame:SetState("Battle")
        end)
      loveframes.SetState("Battle")
      frame:SetState("Battle")
    tut_wildboar = true
    end
  end
end

function self.battleFinish(id)
  if q.state == 3 and id == "battle_river_wildboar" then
    if battle.result == "win" then
      quest.pushBack("main_ch1_3")
      q.state = 4
    elseif battle.result == "lost" then
      battle.makeNextReset()
    end
  end
  
  if id == "battle_trainingcamp_elf" then
    if quest.times["training_camp"] >= 0 and math.random(1,3) >= 1 and battle.result == "win" or true then
      if st.temple == 0 then
        quest.pushBack("main_ch1_chalicetemple_1")
        quest.pushBack("main_ch1_chalicetemple_2")
        quest.pushBack("main_ch1_chalicetemple_3")
      end
      st.temple = st.temple + 1
    else
      if math.random(1,3) >= 2 then
        quest.insert(1, "main_ch1_trainingcamp_battle_more")
      else
        quest.insert(1, "main_ch1_trainingcamp_battle_nomore")
      end
    end
  end
end

return self