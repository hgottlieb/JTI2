local MainMenu = newScene()

function MainMenu:init()
  self.background.image = love.graphics.newImage("image/menubackground.jpg")
  self.title = {
    image = love.graphics.newImage("gui/sprite/title.png"),
    position = {20, 50, 0, 0.7, 0.7}
  }
  
  self.color = {0, 0, 0, 255}
  Timer.after(1, function() Timer.tween(2, self.color, {0, 0, 0, 0}, "linear") end )
  
  if not skip_mainmenu then
    music.source:play()
  end
end

function MainMenu:enter(from)
  if self.nofirst then self.color = {255,255,255,0} end 
  self.nofirst = true
  self:createBasicMenu()
  
  -- Skip main menu
  if skip_mainmenu then
    Timer.finish()
    self.button_newgame.OnClick()
  end
end

function MainMenu:draw()
  scene.camera:attach()
  love.graphics.draw(self.background.image, 0, 0, 0, 0.5, 0.5)
  love.graphics.draw(self.title.image, unpack(self.title.position))
  
  loveframes.draw()
  
  -- Draw the balck fade
  love.graphics.setColor(unpack(self.color))
  love.graphics.draw(byte_image, 0, 0, 0, 960, 540)
  love.graphics.setColor(255, 255, 255, 255)
  
  scene.camera:detach()
end

function MainMenu:createButton(text)
  local button = loveframes.Create("button", frame)
  button:SetText(text)
  button:SetSize(200, 50)
  self.list:AddItem(button)
  return button
end

function MainMenu:createBasicMenu()
  if self.list then self.list:Remove() end
  self.list = loveframes.Create("list")
  self.list:SetPos(210, 250)
  self.list:SetSize(200, 5+(50+5)*4)
  self.list:SetPadding(5)
  self.list:SetSpacing(5)
  
  local button_newgame = self:createButton("New Game")
  self.button_newgame = button_newgame
  
  local button_load = self:createButton("Load")
  local button_config = self:createButton("Options")
  local button_exit = self:createButton("Quit")
  
  -------- Events ---------
  button_newgame.OnClick = function(object, x, y)
    Timer.clear()
    Timer.tween(3, music, {volume=0}, "linear", function()
      music.volume = 1 
      music.source:stop()
    end)
  
    local t = 3
    if skip_mainmenu then t = 0 end
    Timer.tween(t, self.color, {0, 0, 0, 255}, "linear", function()
      player = require("script.player")
      self.list:Remove()
      Gamestate.switch(scene.list.slideshow)
    end)
  end
  
  button_load.OnClick = function(object, x, y)
    -- something here
  end

  button_config.OnClick = function(object, x, y)
    self.list:Clear()
    self:createConfigMenu()
  end
  
  button_exit.OnClick = function(object, x, y)
    displayExitMenu()
  end
end

function MainMenu:createConfigMenu()
  self.list:SetPos(210, 280)
  self.list:SetSize(200, (50+5)*2)
  
  local fullscreen = loveframes.Create("checkbox", frame)
  fullscreen:SetText("Fullscreen")
  fullscreen:SetChecked(love.window.getFullscreen())
  self.list:AddItem(fullscreen)
  
  local debug = loveframes.Create("checkbox", frame)
  debug:SetText("Cheater! Mode")
  self.list:AddItem(debug)
  
  local button_back = self:createButton("Back")
  
  -------- Events ---------
  fullscreen.OnChanged = function(object, bool)
    sui.click:stop()
    sui.click:play()
    love.window.setFullscreen(bool)
    window_flags.fullscreen = bool
    Lady.save_all("config.txt", window_flags, config)
  end
  
  button_back.OnClick = function(object, x, y)
    self.list:Clear()
    self:createBasicMenu()
  end
end

return MainMenu