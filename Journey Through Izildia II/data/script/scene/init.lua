scene = {list={}}

function newScene()
  return {
      background = {image=nil, color={256, 256, 256, 0}},
    }
end

scene.camera = Camera(480,270)
scene.color = {255, 255, 255, 0}

local path = ...
scene.list.mainmenu = require(path .. ".mainmenu")
scene.list.slideshow = require(path .. ".slideshow")
scene.list.map = require(path .. ".map")

return scene