--- The battle module (Global).
--
-- A battle is like any other slide but starts with the "battle" keyword. Enemies are spawned using the special tag "#spawn:enemy-name_". Extra enemies can be spawned at battle time using the appropiate methods.
-- @module battle

local battle = {}

battle.items = require("script.items")
battle.result = nil

local font = love.graphics.newFont("gui/font/Gotham.ttf", 15)
local gui = {func={}}
local npc = {}
local turn = 0
local e_index = 1
local lvl_list = {0.001, 0.02, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1.2, 99.9}
battle.lvl_list = lvl_list

local function limit(state)
  if state[1] > state[2] then state[1] = state[2] end
  if state[1] < 0 then state[1] = 0 end
  return state[1]
end

local function getStateLine(state, update)
  if update then state[1] = state[1] + state[3] end
  state[1] = limit(state)
  return math.floor(state[1]) .. "/" .. state[2]
end

--- Prints a line of text on the battle log.
-- @param text The line of text.
function battle.log(text)
  local itext = scene.list.slideshow.itext
  local n = #itext:GetLines()
  itext:SetLine(n+1, text)
end

local function create_text(text, pos)
  local t = loveframes.Create("text", frame)
  t:SetText({ {color={255,255,255,255}}, text})
  t:SetPos(pos[1],pos[2])
  t:SetFont(font)
  t:SetState("Battle")
  t.UpdateText = function(obj, tmp)
    obj:SetText({ {color={255,255,255,255}}, tmp})
  end
  return t
end


-- Battle START ---------------------------------------------------------------------------
-------------------------------------------------------------------------------------------

--- Changes the slide to battle mode. Automatically called on "slideshow.loadSlide()". It creates all the battle interface and setups everything.
function battle.start()
  turn = 0
  if battle.running then return end
  battle.running = true
  scene.list.slideshow.itext:SetText("")
  scene.list.slideshow.itext:SetAutoScroll(true)
  
  gui = {player = {}, button = {}}
    
  loveframes.SetState("Battle")
  scene.list.slideshow.menubutton:SetState("Battle")
  
  battle.log("Battle begins!\n")
  
  gui.player.mp = create_text("MP: " .. getStateLine(player.mp), {30,510})
  gui.player.hp = create_text("HP: " .. getStateLine(player.hp), {440,510})
  gui.player.sp = create_text("SP: " .. getStateLine(player.sp), {850,510})
  
  gui.menu = control.create_menu("Items", "Stance", "Magic", "Attack")

  gui.menu.b[4].OnClick = function(object, x, y)
    if turn == 0 then battle.log("") end
    if not next(npc) then battle.log("You attack the air. The air looks unharmed!")
    else
      battle.calc_stats(player)
      player:attack(npc[e_index])
    end
    battle.refresh()
    battle.nextTurn()
  end
  
  gui.menu.b[3].OnClick = function(object, x, y)
    battle.log("You know no such thing as magic.")
  end
  
  gui.menu.b[2].OnClick = function(object, x, y)
    gui.menu:Close()
    
    local stance = control.create_menu("M. Protection", "Offensive", "Defensive", "Evasion", "Balanced")
    local returnfunc = function()
      stance:Remove()
      gui.menu:Open()
      control.updateStats()
    end
    
    stance.b[5].OnClick = function()
      battle.log("You take a neutral stance.")
      for i, S in pairs(player.stat) do S[4] = 0 end
      returnfunc()
    end 
    
    stance.b[4].OnClick = function()
      battle.log("You relax your muscles and concentrate on the enemy movements.")
      player.stat.attack[4]     = 1
      player.stat.defense[4]    = -3
      player.stat.evasion[4]    = 3
      player.stat.endurance[4]  = -1
      
      returnfunc()
    end
    
    stance.b[3].OnClick = function()
      battle.log("Your breath deeply as your body becomes a rock.")
      player.stat.attack[4]     = -3
      player.stat.defense[4]    = 3
      player.stat.evasion[4]    = -3
      player.stat.endurance[4]  = 3
      
      returnfunc()
    end
    
    stance.b[2].OnClick = function()
      battle.log("You prepare your weapon and look for openings on the enemy side.")
      player.stat.attack[4]     = 3
      player.stat.defense[4]    = -1
      player.stat.evasion[4]    = -1
      player.stat.endurance[4]  = -1
      
      returnfunc()
    end
    
    stance.b[1].OnClick = function()
      battle.log("You know nothing Jon Snow")
      
      battle.log("You take a neutral stance.")
      for i, S in pairs(player.stat) do S[4] = 0 end
      
      returnfunc()
    end
  end
  
  gui.menu.b[1].OnClick = function(object, x, y)
    battle.log("Your dev is still working on it, so, for now, use your bare hands.")
  end
  
  local nextb = scene.list.slideshow.imagebutton
  nextb.OnClick = function()
    if math.random(0, 9) > 5 then
      battle.log("You escaped succesfully!")
      battle.result = "escaped"
      battle.finish()
    else
      battle.log("You've been intercepted. Escape failed!")
      battle.nextTurn()
    end
  end
  nextb:SetState("Battle")
  
end

--- Called to start a new turn, usually after the player attack.
function battle.nextTurn()
  turn = turn + 1
  
  if next(npc) == nil then
    battle.log("You win!")
    battle.result = "win"
    battle.finish()
  end
  for _, thenpc in pairs(npc) do
    if thenpc ~= nil then thenpc:attack(player) end
  end
  battle.log("")
  
  gui.player.mp:UpdateText("MP: " .. getStateLine(player.mp, true))
  gui.player.hp:UpdateText("HP: " .. getStateLine(player.hp, true))
  gui.player.sp:UpdateText("SP: " .. getStateLine(player.sp, true))
  
  if math.floor(player.hp[1]) <= 0 then
    battle.log("You lose.")
    battle.finish()
  end
  
  if npc[e_index] == nil then
    for i=1,#npc do
      if npc[i] ~= nil then
        npc[i].gui.button:SetImage("gui/simplemark3.png")
        e_index = i
        break
      end
    end
  end
end

--- Called to finish the battle. The result can be checked after this call on "battle.result"
function battle.finish()
  if not battle.running then return end 
  battle.running = false
  
  for i, tnpc in pairs(npc) do
    tnpc.gui.lname:Remove()
    tnpc.gui.hp:Remove()
    tnpc.gui.button:Remove()
    tnpc = {}
  end
  npc = {}
  
  local scn = scene.list.slideshow
  scn.itext:SetAutoScroll(false)
  if scn.itext.vbar then
    scn.itext:update(0.1) -- Recalculate ScrollBody (Or else the battle.log() made this frame won't scroll)
    scn.itext:GetVerticalScrollBody().internals[1].internals[1]:Scroll(50)
  end
  
  gui.player.hp:Remove()
  gui.player.mp:Remove()
  gui.player.sp:Remove()
  
  gui.menu:Remove()
  
  loveframes.SetState("none")
  if control.stats_frame then control.stats_frame:SetState("none") end
  scn.menubutton:SetState("none")
  
  local nextb = scn.imagebutton
  nextb.OnClick = scn.goNext
  nextb:SetState("none")
  
  if math.floor(player.hp[1]) <= 0  then
    battle.result = "lost"
  end
  player.hp[1] = 100
  player.sp[1] = player.sp[2]
  
  Signal.emit("battle-finish", scn.queue[scn.index])
    
end

--- Replaces the next button with a restart battle behavior.
function battle.makeNextReset()
  local scn = scene.list.slideshow
  local nextb = scn.imagebutton
  nextb.OnClick = function()
    scn.index = 0
    
    -- little trick
    table.insert(scn.bbcode.aftercall, function() Effect.addAfterLast("blackFadeOut") end)
    Effect.addBeforeNext("blackFadeIn")
    
    nextb.OnClick = nil
    Effect.runBeforeQueue()
  end
end

--- Refreshes the battle GUI
function battle.refresh()
  gui.player.mp:UpdateText("MP: " .. getStateLine(player.mp))
  gui.player.hp:UpdateText("HP: " .. getStateLine(player.hp))
  gui.player.sp:UpdateText("SP: " .. getStateLine(player.sp))
  
  for i, enemy in pairs(npc) do
    if enemy ~= nil then
      enemy.gui.hp:UpdateText(getStateLine(npc[i].hp))
    end
  end
end

-- Battle END -------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

--- Creates a new NPC. This is a generator function, NPC are further customized on the NPC module.
-- @param name The name of the new NPC.
function battle.newNPC(name)
  local n = {}
  n.name = name
  n.hp = {100, 100, 0}
  n.mp = {0, 0, 0}
  n.sp = {100, 100, 5}
  n.stat = battle.create_stat_table()
  n.stat.evasion[3] = 1
  n.weapon = battle.items["claws"]
  n.armature = {}

  n.attack = function(object, target)
    local damage = 4
    local j = math.random(1,3)
    if j == 1 then battle.log("The " .. object.name .. " attacks " .. target.name .. " with brute force.") end
    if j == 2 then battle.log("The " .. object.name .. " attacks " .. target.name .. " with brute force.") end
    if j == 3 then battle.log("The " .. object.name .. " attacks " .. target.name .. " with brute force.") end
    target:recieveAttack(damage+j*2)
  end
  
  n.recieveAttack = function(object, damage)
    battle.SetNPCHP(object, -damage)
  end
  
  n.defeatedMessage = function(object)
    battle.log(object.name .. " dies in agony.")
  end
  
  n.remove = function(object)
    local i = object.index
    object:defeatedMessage()
    npc[i].gui.button:SetImage("gui/simplemark.png")
    npc[i] = nil
    object = nil
  end
  
  return n
end

--- Used when you need to set an NPC HP. Usually from an attack. It makes sure the HP is beetween the limits and removes the NPC when defeated (HP < 0).
-- @param object The NPC object
-- @param amount The amount of HP to add, negative to substract.
function battle.SetNPCHP(object, amount)
  object.hp[1] = object.hp[1] + amount
  object.hp[1] = limit(object.hp)
  if object.hp[1] == 0 then
    object.gui.hp:UpdateText("Defeated")
    object:remove()
  end
end

--- Spawns a new NPC
-- @param name The name of the NPC. It's the name of the file on the NPC subdirectory.
function battle.spawn(name)
  local tnpc = require("script.npc." .. name)
  table.insert(npc, loveframes.util.DeepCopy(tnpc))
  battle.log("A " .. npc[#npc].name .. " has appeard!")
  
  function create_npc(i, pos)
      local enemy = npc[i]
      local t = {}
      t.lname = create_text(enemy.name, {pos[1]+10, pos[2]+50*i})
      t.hp = create_text(getStateLine(enemy.hp), {pos[1]+10, pos[2] + 50*i + 20})
      
      t.button = loveframes.Create("imagebutton")
      t.button:SetPos(pos[1]-5, pos[2]+50*i - 15)
      t.button:SetSize(200, 60)
      t.button:SetState("Battle")
      t.button:SetText("")
      t.button:SetImage("gui/simplemark.png")
      t.button:SetImageHover("gui/simplemark2.png")
      
      if i == e_index then
        t.button:SetImage("gui/simplemark3.png")
      end
      
      t.button.OnClick = function(object, x, y)
        if npc[i] then
          npc[e_index].gui.button:SetImage("gui/simplemark.png")
          object:SetImage("gui/simplemark3.png")
          e_index = i
        end
      end
      
      return t
  end
  
  npc[#npc].gui = create_npc(#npc, {30, 80})
  npc[#npc].index = #npc
end

--- Geneartes the a new default state table, to use on NPC and the Player.
-- The stat table is composed of 4 stats (attack, defense, evasion and endurance), and 4 numeric columns representing the maxvalue, the peerturn buff, the base level and the level buff for each state. The final column with index "f" represents the actual value of the state on a given turn. The final value is calculated using the "calc_stats" function of this module.
--@usage attack = {200, 0, 2, 0, f=0}
-- defense = {0, 0, 2, 0, f=0}
-- evasion = {100, 0, 2, 0, f=0}
-- endurance = {100, 0, 2, 0, f=0}

function battle.create_stat_table()
  local T = {
    attack =    {200, 0, 2, 0, f=0},
    defense =   {0,   0, 2, 0, f=0},
    evasion =   {100, 0, 2, 0, f=0},
    endurance = {100, 0, 2, 0, f=0},
  }
  return T
end

--- Calculates and refreshes the final values of the state table of a given NPC or Player. Specifically it adds the buffs, calculates the attack and defense given the equiped items and applies the changes to the "f" column of the stats table of the given object.
-- @param obj An spawned NPC object or the Player.
function battle.calc_stats(obj)
  local T = obj.stat
  function calc_stat(S)
    local lvl = S[3]+S[4]
    if lvl < 1 then lvl = 1 end
    if lvl > #lvl_list then lvl = #lvl_list end
    
    S.f = (S[1]+S[2]) * lvl_list[lvl]
  end
  
  T.attack[1] = obj.weapon.damage
  
  local armSum = 0
  for i, x in ipairs(obj.armature) do armSum = armSum + x.defense end 
  
  T.defense[1] = armSum
  
  for i, S in pairs(T) do calc_stat(S) end
end

--- Calculates the stamina consumption the player or a NPC should have given its endurance, the weapon weight and the power of an attack on the current stance.
-- @param obj An spawned NPC or the Player.
-- @return Returns the calculated stamina cosumption where 0 <= stacons <= 100
function battle.calc_stacons(obj)
  local e = obj.stat.endurance.f
  local w = obj.weapon.weight
  local atklbuf = obj.stat.attack[4] 
  
  return math.mkp( (atklbuf*3 + 9 + w) * (100-e)/100) 
end

--- Gets a NPC object from the spawned NPCs on the current battle.
-- @param index The index of the NPC, in order from old to new.
function battle.getNPC(index)
  return npc[index]
end
return battle