local items = {}

function items.create_edible(name, desc, weight, func)
  local T = {}
  T.name = name
  if name == nil then error() end
  
  T.desc = desc or ""
  T.weight = weight or 0
  T.func = func or function() battle.log("This " .. name .. " is delicious.") end
  T.type = "EDI"
  
  items[name] = T
  return T
end

function items.create_weapon(name, desc, weight, damage)
  local T = {}
  T.name = name
  if name == nil then error() end
  
  T.desc = desc or ""
  T.weight = weight or 0
  T.damage = damage or 200
  T.type = "WPN"
  
  items[name] = T
  return T
end

function items.create_armature(name, desc, weight, defense, pos)
  local T = {}
  T.name = name
  if name == nil then error() end
  
  T.desc = desc or ""
  T.weight = weight or 0
  T.defense = defense or 200
  T.type = "ARM"
  T.position = pos or "full"
  
  items[name] = T
  return T
end

-- Create items
items.create_weapon("claws", "Tipical beast claws", 0, 500)
items.create_armature("elf clothes", "A set of basic elf clothes", 2, 40)

return items