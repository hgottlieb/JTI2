local npc = battle.newNPC("Wild Boar")
npc.hp = {200, 200, 5}

function npc.attack(object, target)
  local damage = 2
  local j = math.random(1,3)
  if j == 1 then
    battle.log("The " .. object.name .. " attacks " .. target.name .. " with brute force.")
  elseif j == 2 then
    battle.log("The " .. object.name .. " charges against " .. target.name ..
      " destroying everything in the way.")
  elseif j == 3 then
    battle.log("A " .. object.name .. " rampages arround suqueling at " .. target.name .. ".") 
  end
  target:recieveAttack(damage+j*2)
end

return npc