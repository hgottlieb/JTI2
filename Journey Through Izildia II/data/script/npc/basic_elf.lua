local npc = battle.newNPC("Elf")
npc.hp = {100, 100, 8}

local defense = 4
local state = -1

function npc.attack(object, target)
  state = state + 1
  local damage = 6
  local f = state % 3
  
  if f == 1 then
    battle.log("Your elf rival attacks you with a strong hit!")
    defense = 2.5
  elseif f == 2 then
    battle.log("Your rival adopts an offensive stance.")
    battle.log("An elf attacks with an incredible strong hit!")
    damage = 27
    defense = 1
  elseif f == 0 then
    battle.log("Your training companion changes to a defensive pose.")
    battle.log("She waits and observes your movements.")
    damage = 0
    defense = 5
  end  
  
  target:recieveAttack(damage)
end

function npc.recieveAttack(object, damage)
  if defense == 1 then battle.log("Your adversary recieves the full force of your attack") end
  if defense >= 3 then battle.log("Your adversary has the defense up and mitigates most of the damage") end
  battle.SetNPCHP(object, -damage/defense)
end

function npc.defeatedMessage(object)
  battle.log("Your rival has been defeated, the battle is over.")
end

return npc